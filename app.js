// var http = require('http');

// var server = http.createServer(function(req,res){
// 	console.log('server started');
// });

// server.listen(3000);

var express = require('express');
var app = express();
var fs = require('fs');


inputFile= 'input.txt';
outputFile= 'output.txt';

app.get('/',function(req,res) {
	readFile(inputFile,(err,content)=>{
		writeFile(outputFile,content,(err)=>{
			if(err){
				console.log(err);
				res.send('Failed writing file');
			} else{
				console.log('completed writing file '+outputFile);
				res.send('File write successful');
			}
		});
	})
});

app.listen(3000,()=>{
	console.log('server started');
})

function readFile(inputFile,callback){
	console.log('reading file '+inputFile);
	var contents = fs.readFileSync(inputFile).toString();
	callback(err,contents);
}

function writeFile(outputFile,content,callback){
	fs.writeFile(outputFile,content,function(err){
		if(err){
			callback(err)
		} else{
			callback(null);
		}
	});
}